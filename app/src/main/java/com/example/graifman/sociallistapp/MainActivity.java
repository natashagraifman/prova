package com.example.graifman.sociallistapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista = findViewById(R.id.lista);

        if (isOnline()){
            update_list(lista);
        }
        else{
            workOffline();
        }
    }
    public void update_list(final ListView lista){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://dl.dropboxusercontent.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JSONService service = retrofit.create(JSONService.class);

        service.getSocialList().enqueue(new Callback<SocialList>() {
            @Override
            public void onResponse(Call<SocialList> call, Response<SocialList> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "OIE", Toast.LENGTH_LONG);
                    SocialList socialList = response.body();

                    Gson gson = new Gson();
                    String jsonInString = gson.toJson(socialList);

                    saveInSharedPreferences(jsonInString);

                    ArrayAdapter<Social> adapter = new ArrayAdapter<Social>(MainActivity.this, android.R.layout.simple_list_item_1, socialList.getAcoes_sociais());
                    lista.setAdapter(adapter);
                }
                else {
                    workOffline();
                }
            }

            @Override
            public void onFailure(Call<SocialList> call, Throwable t) {
                workOffline();
            }
        });
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void saveInSharedPreferences(String jsonSocialList) {
        SharedPreferences.Editor editor = getSharedPreferences("social",MODE_PRIVATE).edit();
        editor.putString("social_json", jsonSocialList);
        editor.apply();
    }

    public void workOffline() {
        SharedPreferences preferences = getSharedPreferences("social", MODE_PRIVATE);
        String jsonSocial = preferences.getString("social_json", null);
        Gson gson = new Gson();
        SocialList social_list = gson.fromJson(jsonSocial, SocialList.class);
        ArrayAdapter<Social> adapter = new ArrayAdapter<Social>(MainActivity.this, android.R.layout.simple_list_item_1, social_list.getAcoes_sociais());
        lista.setAdapter(adapter);
    }
}
