package com.example.graifman.sociallistapp;

import java.util.List;

/**
 * Created by rangel on 12/17/17.
 */

public class SocialList {
    private List<Social> acoes_sociais;

    public List<Social> getAcoes_sociais() {
        return acoes_sociais;
    }

    public void setAcoes_sociais(List<Social> acoes_sociais) {
        this.acoes_sociais = acoes_sociais;
    }
}
