package com.example.graifman.sociallistapp;

/**
 * Created by rangel on 12/17/17.
 */

class Social{
    private long id;
    private String name;
    private String image;
    private String description;
    private String site;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    @Override
    public String toString() {
        return this.getName() + "\n" +
                this.getDescription() + "\n" +
                this.getSite();
    }
}
